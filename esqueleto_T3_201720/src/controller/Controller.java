package controller;

import java.io.File;

import model.exceptions.TripNotFoundException;
import model.logic.STSManager;
import api.ISTSManager;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	/**
	 * Carga una lista de paradas a partir de un archivo de texto.
	 */
	public static void loadStops() {
		manager.loadStops("dataT2/stops.txt");		
	}

	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 */
	public static void readBusUpdates() {
		
		System.currentTimeMillis();
		
		File f = new File("data");
		File[] updateFiles = f.listFiles();
		
		for (int i = 0; i < updateFiles.length; i++) {
			manager.readBusUpdate(updateFiles[i]);
		}
		
		System.currentTimeMillis();
	}
	
	/**
	 * Retorna el nombre de todas las paradas por las que paso un viaje especifico. 
	 * @param tripId. Viaje especifico
	 * @throws TripNotFoundException. Excepcion en caso de que no se encuentre el viaje.
	 */
	public static void listStops(Integer tripId) throws TripNotFoundException{
		manager.listStops(tripId);
	}	
}