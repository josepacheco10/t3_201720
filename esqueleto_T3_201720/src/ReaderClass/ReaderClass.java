package ReaderClass;

import java.awt.List;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import model.vo.BusUpdateVO;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class ReaderClass {

	private static double tamanio;
	
	private ArrayList lista;
	
	
	
	public static void readBusUpdate(File rtFile) {
	        
		JSONParser parser = new JSONParser();
		ArrayList lista = new ArrayList<BusUpdateVO>();
		
		ArrayList prueba = new ArrayList();
	        try {
	 
	            JSONArray listaGson = (JSONArray) parser.parse(new FileReader(rtFile));
	            Iterator iter = listaGson.iterator();
	            int z = 0;
	            while (iter.hasNext())
	            {
	            	
	            	
	            	JSONObject miau = (JSONObject) listaGson.get(z);
	            	
	            	String vehicleNo = miau.get("VehicleNo").toString();
	            	String tripID = miau.get("TripId").toString();
	            	String RouteNo = miau.get("RouteNo").toString();
	            	String pDirection = miau.get("Direction").toString();
	            	String pDestination = miau.get("Destination").toString();
	            	String pPattern = miau.get("Pattern").toString();
	            	String pLatitude = miau.get("Latitude").toString();
	            	String pLongitude = miau.get("Longitude").toString();
	            	String pRecord = miau.get("RecordedTime").toString();
	            	String pRutaMap = miau.get("RouteMap").toString();
	            	
	            	BusUpdateVO bus = new BusUpdateVO(vehicleNo, tripID, RouteNo, pDirection, pDestination, 
	            			pPattern, pLatitude, pLongitude, pRecord, pRutaMap);
	            	
	            	
		            lista.add(bus);
	            
		            
		            iter.next(); 
		            z++;
		            
	            }

	 
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        
        	System.out.println(lista.size());
	    }
	
	
	
	public static void main(String[] args) {

		File f = new File("data");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			readBusUpdate(updateFiles[i]);
		}
	}
	
//	public static void main(String[] args){
//
//		String l = "data/BUSES_SERVICE_0.json";
//		readBusUpdate(l);
//		
//	}
	
	
	
        
}
