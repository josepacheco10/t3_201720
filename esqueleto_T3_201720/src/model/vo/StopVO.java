package model.vo;

/**
 * Clase que representa una parada o un Objeto de tipo Stop.
 */
public class StopVO {
	
	/**
	 * Id de la parada.
	 */
	private int stopId;
	
	/**
	 * Codigo de la parada.
	 */
	private int stopCode;
	
	/**
	 * Nombre de la parada.
	 */
	private String stopName;
	
	/**
	 * StopDesc.
	 */
	private String stopDesc;
	
	/**
	 * Latitud de la parada.
	 */
	private double stopLat;
	
	/**
	 * Longitud de la parada.
	 */
	private double stopLon;
	
	/**
	 * Id de la zona de la parada.
	 */
	private String zoneId;
	
	/**
	 * URL de la parada.
	 */
	private String stopUrl;
	
	/**
	 * Location Type.
	 */
	private int locationType;

	
	public StopVO (int pStopId, int pStopCode, String pStopName, String pStopDesc, double pStopLat, double pStopLon, String pZoneId, String pStopUrl, int pLocationType)
	{
		stopId = pStopId;
		stopCode = pStopCode;
		stopName = pStopName;
		stopDesc = pStopDesc;
		stopLat = pStopLat;
		stopLon = pStopLon;
		zoneId = pZoneId;
		stopUrl = pStopUrl;
		locationType = pLocationType;
	}
	
	public int getStopId()
	{
		return stopId;
	}
	
	public int getStopCode()
	{
		return stopCode;
	}
	
	public String getStopName()
	{
		return stopName;
	}
	
	public String getStopDesc()
	{
		return stopDesc;
	}

	public double getStopLat()
	{
		return stopLat;
	}
	
	public double getStopLon()
	{
		return stopLon;
	}
	
	public String getZoneId()
	{
		return zoneId;
	}
	
	public String getStopUrl()
	{
		return stopUrl;
	}
	
	public int getLocationType()
	{
		return locationType;
	}
	
	public String getParentStation()
	{
		return "m";
	}
}
