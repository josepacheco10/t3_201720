package model.vo;

public class BusUpdateVO {

	
	private String vehicleNo;
	
	private String tripID;
	
	private String routeNo;
	
	private String direction;
	
	private String destination;
	
	private String pattern;
	
	private String latitude;
	
	private String longitude;
	
	private String recordTime;
	
	private String routeMap;
	
	
	public BusUpdateVO(String pVehicle, String pTrip, String pRouteNo, String pDirection, String pDestination, String pPattern,
			String pLatitude, String pLongitude, String pRecord, String pRutaMap)
	{
		
		vehicleNo = pVehicle;
		
		tripID = pTrip;
		
		routeNo = pRouteNo;
		
		direction = pDirection;
		
		destination = pDirection;
		
		pattern = pPattern;
		
		latitude = pLatitude;
		
		longitude = pLongitude;
		
		recordTime = pRecord;
		
		routeMap = pRutaMap;
	}
	
	/**
	 * 
	 * @return
	 */
	public String darNoVehicle()
	{
		return vehicleNo;
	}
	
	/**
	 * Metodo encargado de retornar el Id del viaje en cuestion.
	 * @return tripID. ID del viaje.
	 */
	public String getTripID()
	{
		return tripID;
	}
	
	/**
	 * Metodo encargado de retornar el numero de la ruta en cuestion.
	 * @return routeNo. Numero de la ruta.
	 */
	public String getRouteNo()
	{
		return routeNo;
	}
	
	/**
	 * Metodo encargado de retornar la direccion actual de la ruta.
	 * @return direction. Direccion actual de la ruta.
	 */
	public String getDirection()
	{
		return direction;
	}
	
	/**
	 * Metodo encargado de retornar el destino de la ruta.
	 * @return destination. Destino de la ruta.
	 */
	public String getDestination()
	{
		return destination;
	}
	
	/**
	 * Metodo encargado de retornar el modelo del bus en la ruta en cuestion.
	 * @return pattern. Model del bus en una ruta especificada.
	 */
	public String getPattern()
	{
		return pattern;
	}
	
	/**
	 * Metodo encargado de retornar la latitud de la ruta.
	 * @return latitude. Latitud de la ruta.
	 */
	public String getLatitude()
	{
		return latitude;
	}
	
	/**
	 * Metodo encargado de retornar la longitud de la ruta.
	 * @return longitude. Longitud de la ruta.
	 */
	public String getLongitude()
	{
		return longitude;
	}
	
	/**
	 * Metodo encargado de retornar el tiempo registrado.
	 * @return recordTime. Tiempo registrado de la ruta.
	 */
	public String getRecordTime()
	{
		return recordTime;
	}

	/**
	 * Metodo encargado de retornar el mapa de la Ruta.
	 * @return routeMap. Mapa de la ruta.
	 */
	public String darRouteMap()
	{
		return routeMap;
	}
}