package model.data_structures;

/**
 * Clase que representa la interfaz de un Stack.
 * @param <E>. Clase para cumplir con el requerimiento de Generics.
 */

public interface IStack<T> {

	/**
	 * M�todo para enviar un elemento a la Pila.
	 * @param item. Elemento a incluir en la Pila.
	 */
	public void push (T item);
	
	/**
	 * Busca, elimina y retorna un elemento en la Pila.
	 * @return Elemento que se retira de la Pila.
	 */
	public T pop();
	
	
	/**
	 * Retorna un boolena que informa si la Pila se encuentra vacia o no.
	 * @return  True || False. De acuerdo a si la Pila esta vacia o no.
	 */
	public boolean isEmpty();
	
	/**
	 * Retorna la cantidad de elementos existentes en la Pila.
	 * @return Elementos en la Pila.
	 */
	public int size();
}
