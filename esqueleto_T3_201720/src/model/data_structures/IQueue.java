package model.data_structures;

/**
 * Clase que representa la interfaz de una cola.
 * @param <E>. Clase para cumplir con el requerimiento de Generics.
 */
public interface IQueue<T> {
	
	/**
	 * M�todo para ingresar un elemento T a la cola.
	 * @param nElement. Elemento a agregar.
	 */
	public void enqueue(T nElement);
	
	/**
	 * Busca, elimina y retorna un elemento en la Cola.
	 * @return T nElement. Elemento eliminado de la Cola.
	 */
	public T dequeue();

	/**
	 * Retorna un boolena que informa si la Cola se encuentra vacia o no.
	 * @return  True || False. De acuerdo a si la Cola esta vacia o no.
	 */
	public boolean isEmpty();
	
	/**
	 * Retorna la cantidad de elementos existentes en la Cola.
	 * @return Elementos en la Cola.
	 */
	public int size();
}
