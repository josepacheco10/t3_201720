package model.data_structures;

import java.util.Iterator;

public class RingList<T> implements IList<T> {

	/**
	 * Cabeza de la lista, primer elemento.
	 */
	private Node<T> first;

	/**
	 * Ultimo elemento de la lista.
	 */
	private Node<T> last;

	/**
	 * Elemento actual en la lista.
	 */
	private Node<T> actual;

	/**
	 * Elemento sucesor en la lista.
	 */
	private Node<T> sig;

	/**
	 * Elemento sucesor en la lista.
	 */
	private Node<T> prev;

	/**
	 * Tamanio de la lista, cantidad de nodos.
	 */
	private Integer size;
	
	
	public RingList() {
		first = null;
		last = null;
		actual = null;
		sig = null;
		prev = null;
		size = 0;
	}
	
	/**
	 * Retorna el tamanio de la lista.
	 * @return Tamanio de la lista.
	 */
	public Integer getSize() {
		return size;
	}
	
	/**
	 * Informa si la lista se encuentra vacia,
	 * @return True si la lista esta vacia.
	 */
	public boolean isEmpty() {
		boolean iE = false;

		if (first == null) {
			iE = true;
		}
		return iE;
	}
	
	/**
	 * Retorna la cabeza de la lista, el primer elemento.
	 * @return Primer elemento en toda la lista.
	 */
	public Node<T> getFirst() {
		return isEmpty() ? null : first;
	}

	/**
	 * Retorna el �ltimo elemento de la lista.
	 * @return Ultimo elemento en toda la lista.
	 */
	public Node<T> getLast() {
		return isEmpty() ? null : last;
	}
	

	public void add(T nElement) {
		
		Node<T> nuevo = new Node<T>(null, null, nElement);

		if (isEmpty()) {
			
			first = nuevo;
			first.setPrevious(null);
			first.setNext(null);
			
			actual = first;
			
			size++;
		} else {
			
			
			nuevo.setNext(first);
			//last.setNext(first);
			nuevo.setPrevious(last);
			
			first = nuevo;

			size++;
		}
	}

	public void addAtk(T nElement, int pos) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void addLast(T nElement) {
		
		Node<T> add = new Node<T>(null, null, nElement);

		if(first == null)
		{
			first = add;
			last = add;
			size++;
		}
		else 
		{
			add.next = first;
			add.previous = last;
			last.next = add;
			first.previous = add;
			last = add;
			size++;
		}	
	}

	public void delete(Node<T> nElement) throws Exception {

		if (first.item.equals(nElement))
		{ 
			Node<T> save = first.next;
			last.next = save;
			save.previous = last;
			first = save;
			
			size--;
		}
		else if (last.item.equals(nElement))
		{
			Node<T> save = last.previous;
			save.next = first;
			first.previous = save;
			last = save;
			
			size--;
		}
		else
		{
			Node<T> save = first;
				while (save != null)
				{
					if (save.item.equals(nElement))
					{
						Node<T> saveNext = save.next;
						Node<T> savePrev = save.previous;
						saveNext.previous = savePrev;
						savePrev.next = saveNext;
						break;
					}
					save = save.next;
				}
			size--;
			}
	}

	public void deleteAtk(Node<T> nElement, int pos) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public T getElement(Integer indice) {
		
	Node<T> respuesta = null;
		
		if (indice==0||indice%size==0)
		{
			respuesta = first;
		}
		else if(indice>size)
		{
			int posN = indice%size; 
			int contador=0;
			Node<T> aux = first;
		
			while(aux!=null)
			{
			if (contador==posN )
			{
				respuesta = aux;
				break;
			}
			aux = aux.next;
			contador++;
			}
		}
		
		else {
		
			int contador=0;
			Node<T> aux = first;
		
			while(aux!=null)
			{
			if (contador == indice )
			{
				respuesta = aux;
				break;
			}
			aux = aux.next;
			contador++;
			}
		}
		
		return respuesta.item;
	}
	

	public T getCurrentElement() {
		
		T item = actual.item;
		actual = actual.next;
		return item;
	}

	/**
	 * Retorna el elemento anterior.
	 */
	public Node<T> next() {
		return sig;
	}

	/**
	 * Retorna el elemento anterior.
	 */
	public Node<T> previous() {
		return prev;
	}

	//-------------------------------------
	// Iterador.
	//-------------------------------------
	public Iterator<T> iterator() {
		return new RingListIterator();
	}

	private class RingListIterator implements Iterator<T> {

		Node<T> actual = first;

		public boolean hasNext() {
			return actual != null;
		}

		public T next() {
			T item = actual.item;
			actual = actual.next;
			return item;
		}

		public void remove() {
			throw new UnsupportedOperationException();

		}
	}
}