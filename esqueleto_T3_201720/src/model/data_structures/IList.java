package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects This ADT should contain the
 * basic operations to manage a list add, addAtEnd, AddAtK, getElement,
 * getCurrentElement, getSize, delete, deleteAtK 
 * next, previous
 * @param <T>
 */

public interface IList<T> extends Iterable {

	/**
	 * Retorna true si la lista esta vacia, falso de lo contrario.
	 */
	public boolean isEmpty();

		
	////////////////////////////////////////////////////
	//					   ADD						////
	////////////////////////////////////////////////////
	
	/**
	 * Inserta un nuevo elemento en la cabeza de la lista.
	 * @param nElement. Elemento a agregar.
	 */
	public void add (T nElement);

	/**
	 * Inserta un elemento a la lista en una posición dada.
	 * @param nElement. Elemento a agregar.
	 * @param pos. Posición en donde se agregará el elemento.
	 * @throws Exception 
	 */
	public void addAtk (T nElement, int pos) throws Exception;

	/**
	 * Agrega un nuevo elemento en el final de la lista.
	 * @param nElement. Nuevo nodo a agregar en la lista.
	 */
	public void addLast(T nElement);

	
	////////////////////////////////////////////////////
	//					   DELETE					////
	////////////////////////////////////////////////////

	/**
	 * Inserta un nuevo elemento en la cabeza de la lista.
	 * @param nElement. Elemento a agregar.
	 */
	public void delete(Node<T> nElement) throws Exception;

	/**
	 * Elimina un elemento a la lista en una posición dada.
	 * @param nElement. Elemento a eliminar.
	 * @param pos. Posición en donde se encuentra el nodo a eliminar.
	 */
	public void deleteAtk (Node<T> nElement, int pos) throws Exception;

	
	////////////////////////////////////////////////////
	//					   GET						////
	////////////////////////////////////////////////////
	
	/**
	 * Retorna el tamanio de la lista, es decir, la cantidad de nodos.
	 */
	public Integer getSize();
	
	/**
	 * Busca el elemento nElement en la lista y lo retorna.
	 * @param nElement. Elemento que se desea obtener.
	 * @return Elemento encontrado.
	 */
	public T getElement(Integer pos);

	/**
	 * Retorna el nodo actual de la lista.
	 * @return Nodo actual de la lista.
	 */
	public T getCurrentElement();
		
	////////////////////////////////////////////////////
	//				   NEXT & PREV					////
	////////////////////////////////////////////////////

	/**
	 * Retorna el Nodo sucesor al Nodo actual.
	 * @return Nodo sucesor al actual.
	 */
	public Node<T> next();

	/**
	 * Retorna el Nodo predecesor al Nodo actual.
	 * @return Nodo predecesor al actual.
	 */
	public Node<T> previous();
	
	
}