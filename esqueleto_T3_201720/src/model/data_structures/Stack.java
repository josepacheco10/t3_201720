package model.data_structures;

public class Stack<T> implements IStack<T> {
	
	private Integer size;
	
	/**
	 * Cabeza del Stack.
	 */
	private Node<T> first;
	
	/**
	 * Inner class.
	 * @param <T>. Requerimiento para Generics.
	 */
	private class Node<T>
	{
		T item;
		Node <T> next;
	}
	
	public Stack()
	{
		size = 0;
	}
	
	public boolean isEmpty()
	{
		return first == null? true: false;
	}
	
	
	public void push(T nElement)
	{
		Node<T> oldfirst = first;
		first = new Node<T>();
		first.item = nElement;
		first.next = oldfirst;
		
		size++;
	}
	
	
	public T pop()
	{
		T nElement = first.item;
		first = first.next;
		
		size--;
		return nElement;
	}

	public int size() {
		return size;
	}
	
	public T getHead()
	{
		return first.item;
	}
	
}
