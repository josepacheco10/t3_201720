package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue<T>{

	/**
	 * Primer elemento de la Cola.
	 */
	private Node<T> first;
	
	/**
	 * Ultimo elemento de la Cola.
	 */
	private Node<T> last;
	
	/**
	 * Tama�o de la Cola.
	 */
	private Integer size;
	
	public Queue()
	{
		size = 0;
	}
	
	/**
	 * Inner class.
	 */
	private class Node<T>
	{
		T item;
		Node <T> next;
	}
	
	public boolean isEmpty()
	{
		return first == null ? true: false;
	}
	
	public void enqueue(T nElement)
	{
		Node<T> oldLast = last;
		last = new Node<T>();
		last.item = nElement;
		last.next = null;
		
		if (isEmpty())
		{
			first = last;
			size++;
		}
		
		else
		{
			oldLast.next = last;
			size++;
		}
	}
	
	public T dequeue()
	 {
		T item = first.item;
		first = first.next;
		if (isEmpty()) last = null;
		
		
		size--;
		return item;
	}
	
	public int size()
	{
		return size;
	}
	
	public T getFirst()
	{
		return first.item;
	}
	
	
	public T getElement(Integer indice) {
		
		Node<T> respuesta = null;
		boolean fin = false;
			if (indice==0)
			{
				respuesta = first;
			}
			
			else if(indice > 0)
			{
				int posN = indice; 
				int contador=0;
				
				Node<T> aux = first;
			
				while(aux != null && !fin)
				{
					if (contador == posN )
					{
						respuesta = aux;
						fin = true;
					}
					
					aux = aux.next;
					contador++;
				}
			}
			return respuesta.item;
		}
	
	//-------------------------------------
	// Iterador.
	//-------------------------------------
		public Iterator<T> iterator() {
			return new RingListIterator();
		}

		private class RingListIterator implements Iterator<T> {

			Node<T> actual = first;

			public boolean hasNext() {
				return actual != null;
			}

			public T next() {
				T item = actual.item;
				actual = actual.next;
				return item;
			}

			public void remove() {
				throw new UnsupportedOperationException();

			}
		}
}
