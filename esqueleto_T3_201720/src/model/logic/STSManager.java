package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import model.data_structures.IList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.RingList;
import model.data_structures.Stack;
import model.exceptions.TripNotFoundException;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

public class STSManager implements ISTSManager {

	private Queue<BusUpdateVO> cola;
	private IStack<String> pilaStops;
	private IList<StopVO> ringStops;

	private Queue<String>cola2;
	public STSManager()
	{
		cola = new Queue<BusUpdateVO>();
		ringStops  = new RingList<StopVO>();
		pilaStops = new Stack<String>();
		cola2 = new Queue<String>();
	}
	
	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 */
	public void readBusUpdate(File rtFile) {

		JSONParser parser = new JSONParser();
		try {

			JSONArray listaGson = (JSONArray) parser.parse(new FileReader(rtFile));
			Iterator iter = listaGson.iterator();
			int z = 0;
			
			while (iter.hasNext()) {

				JSONObject jsonObject = (JSONObject) listaGson.get(z);

				String vehicleNo = jsonObject.get("VehicleNo").toString();
				String tripID = jsonObject.get("TripId").toString();
				String RouteNo = jsonObject.get("RouteNo").toString();
				String pDirection = jsonObject.get("Direction").toString();
				String pDestination = jsonObject.get("Destination").toString();
				String pPattern = jsonObject.get("Pattern").toString();
				String pLatitude = jsonObject.get("Latitude").toString();
				String pLongitude = jsonObject.get("Longitude").toString();
				String pRecord = jsonObject.get("RecordedTime").toString();
				String pRutaMap = jsonObject.get("RouteMap").toString();

				BusUpdateVO bus = new BusUpdateVO(vehicleNo, tripID, RouteNo,
						pDirection, pDestination, pPattern, pLatitude,
						pLongitude, pRecord, pRutaMap);

				cola.enqueue(bus);

				// Avances
				iter.next();
				z++;
			}	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Retorna el nombre de todas las paradas por las que paso un viaje especifico. 
	 * @param tripId. Viaje especifico
	 * @throws TripNotFoundException. Excepcion en caso de que no se encuentre el viaje.
	 */
	public IStack<String> listStops(Integer tripID) throws TripNotFoundException{
		
		BusUpdateVO busBuscado = null;
		Iterator<BusUpdateVO> iter = cola.iterator();
		int z = 0;
		String tripIDS = String.valueOf(tripID);
		
		while(iter.hasNext()){
			
			String pId = cola.getElement(z).getTripID();
			
			if (pId.equals(tripIDS))
			{
				busBuscado = cola.getElement(z);
				break;
			}
			
			//Avances
			iter.next();
			z++;
		}
		
		if (busBuscado == null)
		{
			throw new TripNotFoundException();
		}
		
		else
		{
			double lat1 = Double.valueOf(busBuscado.getLatitude());
			double lon1 = Double.valueOf(busBuscado.getLongitude());
			
			Iterator<StopVO> iterList = ringStops.iterator();
			int y = 0;
			
			while (iterList.hasNext()){	
					
				double lat2 = ringStops.getElement(y).getStopLat();
				double lon2 = ringStops.getElement(y).getStopLon();
				
				double distancia = getDistance(lat1, lon1, lat2, lon2);
						
				if (distancia == 70)
					{pilaStops.push(ringStops.getElement(y).getStopName());}
				
				else 
					{pilaStops.push("Miau");}
						
			// Avances Lista			
			iterList.next();
			y++;
			}
		}
		return pilaStops;
	}

	/**
	 * Carga una lista de paradas a partir de un archivo de texto.
	 */
	public void loadStops(String stopsFile) {

		BufferedReader br = null;

		try {

			br = new BufferedReader(new FileReader(stopsFile));
			String line = br.readLine();
			line = br.readLine();

			while (line != null) {
				String[] fields = line.split(",");

				int stopId = Integer.parseInt(fields[0]);
				int stopCode = fields[1].equals(" ") ? 000 : Integer.parseInt(fields[1]);
				String stopName = fields[2];
				String stopDesc = fields[3];
				double stopLat = Double.valueOf(fields[4]);
				double stopLon = Double.valueOf(fields[5]);
				String zoneId = fields[6];
				String stopUrl = fields[7];
				int locationType = Integer.parseInt(fields[8]);

				StopVO nueva = new StopVO(stopId, stopCode, stopName, stopDesc,
						stopLat, stopLon, zoneId, stopUrl, locationType);

				ringStops.add(nueva);				
				line = br.readLine();
			}
		} 
		
		catch (IOException e) {
			e.printStackTrace();
		}

		finally {
			if (null != br) {
				try {
					br.close();
				} 
				
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
	
	public double getDistance(double lat1, double lon1, double lat2, double lon2) 
	{
       final int R = 6371*1000; // Radious of the earth
    
        double latDistance = toRad(lat2-lat1);
        double lonDistance = toRad(lon2-lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = R * c;
         
        return distance;
    }
	

	private Double toRad(double value) {
        return value * Math.PI / 180;
    }

}