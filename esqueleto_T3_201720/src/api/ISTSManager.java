package api;

import java.io.File;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.exceptions.TripNotFoundException;
import model.vo.StopVO;

public interface ISTSManager {

	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 */
	public void readBusUpdate(File rtFile);
	
	
	/**
	 * Retorna el nombre de todas las paradas por las que paso un viaje especifico. 
	 * @param tripId. Viaje especifico
	 * @throws TripNotFoundException. Excepcion en caso de que no se encuentre el viaje.
	 */
	public IStack<String> listStops (Integer tripID) throws TripNotFoundException;

	
	/**
	 * Carga una lista de paradas a partir de un archivo de texto.
	 */
	public void loadStops(String pStops);
	
	
}
