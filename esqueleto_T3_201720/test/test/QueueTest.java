package test;

import model.data_structures.Node;
import model.data_structures.Queue;
import junit.framework.TestCase;

public class QueueTest extends TestCase {
	private Queue<Integer> queue;

	public void setupEscenario1() {
		queue = new Queue<Integer>();
	}

	public void setupEscenario2() {
		setupEscenario1();
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		queue.enqueue(6);
		queue.enqueue(7);
		queue.enqueue(8);
		queue.enqueue(9);
		queue.enqueue(10);
	}

	public void isEmptyTest() {
		setupEscenario1();
		assertEquals("Error", 0, queue.size());
	}

	public void enqueueTest() {
		setupEscenario2();
		Node<Integer> nElement = new Node<Integer>(null, null, 8);
		queue.enqueue(nElement.item);
		
		assertEquals("Enqueue Error", 11, queue.size());
	}

	public void dequeueTest() {
		setupEscenario2();
		queue.dequeue();
		assertEquals("Dequeue Error", 9, queue.size());
	}

	public void getElementTest() {
		setupEscenario2();
		assertEquals("getElement Error", 5, queue.getElement(5).intValue());
	}
}