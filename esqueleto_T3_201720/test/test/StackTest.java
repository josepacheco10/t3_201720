package test;

import model.data_structures.Node;
import model.data_structures.Stack;
import junit.framework.TestCase;

public class StackTest extends TestCase {
	
	private Stack<Integer> stack;

	public void setupEscenario1() {
		stack = new Stack<Integer>();
	}

	public void setupEscenario2() {
		setupEscenario1();
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		stack.push(6);
		stack.push(7);
		stack.push(8);
		stack.push(9);
		stack.push(10);
	}

	public void isEmptyTest() {
		setupEscenario1();
		assertEquals("Error", 0, stack.size());
	}

	public void pushTest() {
		setupEscenario2();
		Node<Integer> nElement = new Node<Integer>(null, null, 4);
		
		stack.push(nElement.item);
		assertEquals("Push Error", 11, stack.size());
	}

	public void popTest() {
		setupEscenario2();
		stack.pop();
		assertEquals("Push Error", 9, stack.size());
	}
}